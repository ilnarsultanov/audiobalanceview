package ru.ilnarsoultanov.audiobalanceview.customView;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

import ru.ilnarsoultanov.audiobalanceview.R;

/**
 * Created by skull on 08.11.2018.
 */

public class AudioBalanceView extends View {
    private Paint ovalPaint;
    private float ovalStrokeWidth = 1f;
    private RectF ovalView;
    private float ovalHeight = 90f;

    private Bitmap thumb;
    private Paint thumbPaint;
    private Point lastThumbPoint;
    private int maxValue = 18;
    private int minValue = 0;

    private int currentLeftValue, currentTopValue;

    private OnSeekBarChangeListener mChangListener;

    public AudioBalanceView(Context context) {
        super(context);
        init(context, null);
    }

    public AudioBalanceView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public AudioBalanceView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, @Nullable AttributeSet attributeSet){
        if (attributeSet != null){
            TypedArray a = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.AudioBalanceView, 0, 0);
            try {
                ovalStrokeWidth = a.getDimension(R.styleable.AudioBalanceView_ovalLineWidth, TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1f, getResources().getDisplayMetrics()));
            } finally {
                a.recycle();
            }
        }

        thumb = BitmapFactory.decodeResource(getResources(), R.drawable.shar);

        ovalPaint = new Paint();
        ovalPaint.setAntiAlias(true);
        ovalPaint.setColor(Color.WHITE);
        ovalPaint.setStrokeWidth(ovalStrokeWidth);
        ovalPaint.setStyle(Paint.Style.STROKE);


        thumbPaint = new Paint();
        thumbPaint.setAntiAlias(true);
    }

    @Override
    public void layout(int l, int t, int r, int b) {
        super.layout(l, t, r, b);

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    public void draw(Canvas canvas) {
        Point point = balanceToPoint(currentLeftValue, currentTopValue);

        setOvalViewParams(getLeft() + getPaddingStart(), point.y - ovalHeight , getRight() - getPaddingEnd(), point.y);

        if (ovalView != null){

            // int[] colors = new int[]{ Color.parseColor("#000000"), Color.parseColor("#ffffff"), Color.parseColor("#000000")};
           // Shader gradient = new SweepGradient(ovalView.left, ovalView.bottom - (ovalView.height() / 2), Color.parseColor("#ffffff"), Color.parseColor("#b2000000"));
           // ovalPaint.setShader(gradient);


            drawOvalView(canvas, ovalView, ovalPaint);
        }

        try {
            int a = (int) ovalView.width() / 2;
            int b = (int) (ovalView.height() / 2);
            float x = point.x - (getWidth() / 2) - (getPaddingStart() / 2) + (getPaddingEnd() / 2);
            float[] yY = getEllipseY(a, b, x);
            int y = (int) (yY[0] + ovalView.top  + (ovalView.height() / 2) - (thumb.getHeight() / 2));

            lastThumbPoint = new Point((int)point.x - (thumb.getWidth() / 2), y);
        } catch (Exception e){
            e.printStackTrace();
        }

        if (lastThumbPoint != null){
            drawThumb(canvas, lastThumbPoint);
            //testPointPaint(canvas, lastThumbPoint);
        }

        super.draw(canvas);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        if (isTouchThumb(x, y)) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                setThumb(R.drawable.shar_strelka);
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                setThumb(R.drawable.shar);
            }


            if (x >= getPaddingStart() && x <= getWidth() - getPaddingEnd()
                    && y >= getPaddingTop()
                    && y <= getHeight() - getPaddingBottom()) {

                updateBalance(x, y);
            }

            invalidate();
        }
        return super.onTouchEvent(event);
    }

    private void drawOvalView(Canvas canvas, RectF ovalView, Paint ovalPaint){
        canvas.drawOval(ovalView, ovalPaint);
    }

    private void setOvalViewParams(float left, float top, float right, float bottom){
        ovalView = new RectF(left, top, right, bottom);
    }

    private void drawThumb(Canvas canvas, Point point){
        Log.d("vill", "drawThumb x: " + point.x + " y: " + point.y);
        canvas.drawBitmap(thumb, point.x, point.y, thumbPaint);
    }

    public float[] getEllipseY (int a, int b, double x) throws IllegalArgumentException{
        if (x > a || x < -a) throw new IllegalArgumentException(x + " is out of range" );
        float result = (float) (b * Math.sqrt(1 - x*x/(a*a)));
        return new float[]{result, - result};
    }


    public void setOnSeekBarChangeListener(OnSeekBarChangeListener listener) {
        mChangListener = listener;
    }

    public interface OnSeekBarChangeListener {
        void onChanged(AudioBalanceView view, int left, int top);
    }

    public void setCurrentBalance(int left, int top){
        currentLeftValue = left;
        currentTopValue = top;
        invalidate();
    }

    private void updateBalance(float pointX, float pointY){
        float x1 = pointX - getPaddingStart();
        float y1 = pointY - getPaddingTop();

        float stepVertical = (getHeight() - getPaddingTop() - getPaddingBottom()/*ovalHeight*/) / maxValue;
        float stepHorizontal = (getWidth() - getPaddingStart() - getPaddingEnd()) / maxValue;

        currentTopValue = Math.round(y1 / stepVertical);
        currentLeftValue = Math.round(x1 / stepHorizontal);

        if (mChangListener != null){
            mChangListener.onChanged(this, currentLeftValue, currentTopValue);
        }

        Log.d("balance", "currentTopValue: " + currentTopValue + " currentLeftValue: " + currentLeftValue);
    }

    private Point balanceToPoint(float left, float top){
        float x1 = getPaddingStart();
        float y1 = getPaddingTop();

        float stepVertical = (getHeight() - getPaddingTop() - getPaddingBottom()/*ovalHeight*/) / maxValue;
        float stepHorizontal = (getWidth() - getPaddingStart() - getPaddingEnd()) / maxValue;

        float currentVertical = y1 + (stepVertical * top);
        float currentHorizontal = x1 + (stepHorizontal * left);

        return new Point((int)currentHorizontal, (int)currentVertical);
    }

    public void setThumb(@DrawableRes int thumbResourceId){
        thumb = BitmapFactory.decodeResource(getResources(), thumbResourceId);
    }

    public boolean isTouchThumb(float x, float y){
        int thumbRadius = Math.max(thumb.getWidth() / 2, thumb.getHeight() / 2);

        return true;
    }
}

