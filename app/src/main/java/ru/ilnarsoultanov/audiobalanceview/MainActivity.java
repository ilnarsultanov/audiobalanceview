/*
 * Copyright (c) 2018.
 * http://ilnarsoultanov.ru
 * ilnar.ltd@gmail.com
 */

package ru.ilnarsoultanov.audiobalanceview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import ru.ilnarsoultanov.audiobalanceview.customView.AudioBalanceView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AudioBalanceView audioBalanceView = ((AudioBalanceView) findViewById(R.id.audio));


        audioBalanceView.setOnSeekBarChangeListener(new AudioBalanceView.OnSeekBarChangeListener() {
            @Override
            public void onChanged(AudioBalanceView view, int left, int top) {
                ((TextView) findViewById(R.id.text)).setText("left: " + left +" top: " + top);
            }
        });
        audioBalanceView.setCurrentBalance(18,18);
    }
}
